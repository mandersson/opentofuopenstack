# Create an instance
resource "openstack_compute_instance_v2" "webservers" {
  name            = "web${count.index}"
  count           = var.webreplicas
  image_id        = resource.openstack_images_image_v2.debian12.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = [ for k,v in resource.openstack_compute_keypair_v2.demo-keys: v.name if strcontains(v.name,"primary") ][0]
  security_groups = ["default",resource.openstack_networking_secgroup_v2.sshaccess.name]

  network {
    port   = resource.openstack_networking_port_v2.webpgport[count.index].id
  }
  network {
    port   = resource.openstack_networking_port_v2.webport[count.index].id
  }
  user_data       = templatefile("webnodes-user.tpl",{keys = values(resource.openstack_compute_keypair_v2.demo-keys), cosmosdeb = data.local_file.cosmos.content_base64})
}

resource "openstack_networking_port_v2" "webpgport" {
  name                = "webpg${count.index}_port"
  # We create as many ports as there is web servers.
  count               = var.webreplicas
  # We use the same isolated network as for the pgnodes
  network_id          = resource.openstack_networking_network_v2.privnet[4].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id, resource.openstack_networking_secgroup_v2.pgclient.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[4].id
    ip_address          = "10.0.5.${count.index+50}" # Client offset of 50 in ip range
  }
}

resource "openstack_networking_port_v2" "webport" {
  name                = "web${count.index}_port"
  # We create as many ports as there is servers.
  count               = var.webreplicas
  # We use a isolated private network.
  network_id          = resource.openstack_networking_network_v2.privnet[3].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id, resource.openstack_networking_secgroup_v2.websrv.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[3].id
    ip_address          = "10.0.4.${count.index+10}"
  }
}
