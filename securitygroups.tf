resource "openstack_networking_secgroup_v2" "sshaccess" {
  name        = "sshaccess"
  description = "Allow SSH acces."
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "sshrule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sshaccess.id
}

resource "openstack_networking_secgroup_v2" "pgserver" {
  name        = "pgaccess"
  description = "Allow postgresql access."
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "pgrule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_group_id   = resource.openstack_networking_secgroup_v2.pgclient.id
  security_group_id = openstack_networking_secgroup_v2.pgserver.id
}

# Attach this dummy security group on ports that should be able to access pgnodes
resource "openstack_networking_secgroup_v2" "pgclient" {
  name        = "pgclient"
  description = "Place this group on pg clients."
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_v2" "websrv" {
  name        = "websrv"
  description = "Allow webserver access."
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "webrule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_group_id   = resource.openstack_networking_secgroup_v2.webclient.id
  security_group_id = openstack_networking_secgroup_v2.websrv.id
}

# Attach this dummy security group on ports of reverse proxys that should be able to access webservers
resource "openstack_networking_secgroup_v2" "webclient" {
  name        = "webclient"
  description = "Place this group on web clients."
  delete_default_rules = "true"
}


# We need to fetch the id of the default security group to be able to use it 
# with network ports
data "openstack_networking_secgroup_v2" "defaultsg" {
  name = "Default"
}


