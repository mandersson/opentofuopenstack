# Create an instance
resource "openstack_compute_instance_v2" "backup" {
  name            = "backup"
  image_id        = resource.openstack_images_image_v2.debian12.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = [ for k,v in resource.openstack_compute_keypair_v2.demo-keys: v.name if strcontains(v.name,"primary") ][0]
  security_groups = ["default"]

  network {
    port          = resource.openstack_networking_port_v2.backupweb_port.id
  }
  network {
    port          = resource.openstack_networking_port_v2.backuppg_port.id
  }
  network {
    port          = resource.openstack_networking_port_v2.backuppublic_port.id
  }
  user_data       = templatefile("backup-user.tpl",{keys = values(resource.openstack_compute_keypair_v2.demo-keys), cosmosdeb = data.local_file.cosmos.content_base64, lvmscript = data.local_file.setuplvmbackup.content_base64})
}


resource "openstack_networking_port_v2" "backupweb_port" {
  name                = "backupweb_port"
  # We use a isolated private network.
  network_id          = resource.openstack_networking_network_v2.privnet[3].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[3].id
    ip_address          = "10.0.4.6"
  }
}

resource "openstack_networking_port_v2" "backuppg_port" {
  name                = "backuppg_port"
  # We use a isolated private network.
  network_id          = resource.openstack_networking_network_v2.privnet[4].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[4].id
    ip_address          = "10.0.5.6"
  }
}

resource "openstack_networking_port_v2" "backuppublic_port" {
  name                = "backuppg_port"
  network_id          = resource.openstack_networking_network_v2.privnet[0].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[0].id
    ip_address          = "10.0.1.6"
  }
}


resource "openstack_blockstorage_volume_v3" "backupvolume1" {
  name        = "backupvolume1"
  description = "Used with the backup volumegroup."
  size        = 10
  enable_online_resize = true
  }

resource "openstack_compute_volume_attach_v2" "backupvolattach1" {
  instance_id = openstack_compute_instance_v2.backup.id
  volume_id   = openstack_blockstorage_volume_v3.backupvolume1.id
}

data "local_file" "setuplvmbackup" {
  filename = "${path.module}/backup-setuplvm.sh"
}
