#cloud-config

system_info:
  default_user:
    name: root
disable_root: false
ssh_authorized_keys:
%{ for key in keys ~}
  - ${chomp(key.public_key)}
%{ endfor ~}
packages:
  - chrony
  - git
  - lvm2
write_files:
  - path: /etc/chrony/chrony.conf
    permissions: "0644"
    content: |
%{ for line in split("\n",file("chrony.conf")) ~}
      ${line}
%{ endfor ~}
    owner: root:root
  - path: /root/cosmos_1.5-2~sunet20220414_all.deb
    permission: "0644"
    encoding: base64
    content: |
      ${indent(6,cosmosdeb)}
    owner: root:root
runcmd:
  - [ systemctl, enable, chronyd ]
  - systemctl restart chronyd
  - chronyc sources
