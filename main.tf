
# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

provider "openstack" {
    # Source application credentials to use environemnt variables to provide auth
}

# Upload an image from a local file path
resource "openstack_images_image_v2" "debian12" {
  name             = "Debian12"
  local_file_path  = "/home/magnus/debian-12-genericcloud-amd64-20230910-1499.qcow2"
  container_format = "bare"
  disk_format      = "qcow2"
  min_disk_gb      = 5
}

# If we use an existing image we 
# look up the image ID as a data source
#data "openstack_images_image_v2" "image" {
#  name        = "Debian12" # Name of image to be used
#  most_recent = true
#}

## Get flavor id
data "openstack_compute_flavor_v2" "flavor" {
  name = "ds2G" # flavor to be used
}

data "local_file" "cosmos" {
  filename = "${path.module}/../cosmos_1.5-2~sunet20220414_all.deb"
}

resource "openstack_compute_keypair_v2" "demo-keys" {
  for_each    = fileset(path.module, "../pubkeys/*.pub")
  public_key  = file("${path.module}/${each.key}")
  name        = "demo-${replace(replace(regex("[^/]+$",each.key),".","-"),"_","-")}"
}

# Display rendered cloudinit template.
output "cloudinit" {
  value = nonsensitive(templatefile("mgmt-user.tpl",{keys = values(resource.openstack_compute_keypair_v2.demo-keys), cosmosdeb = data.local_file.cosmos.content_base64}))
}
