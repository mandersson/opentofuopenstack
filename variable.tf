# Variables
variable "keypair" {
  type    = string
  default = "demopub"   # name of keypair created 
}

variable "network" {
  type    = string
  default = "private" # default network to be used
}

variable "pgnodes" {
  description = "Default node names for postgresql"
  type        = list(string)
  default     = ["pgnode1","pgnode2"]
}

variable "webreplicas" {
  type    = number
  default = 3
}
