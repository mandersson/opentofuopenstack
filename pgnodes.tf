# Create an instance
resource "openstack_compute_instance_v2" "pgservers" {
  name            = each.value
  for_each        = toset(var.pgnodes) # Iterate over a list of server names.
  image_id        = resource.openstack_images_image_v2.debian12.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = [ for k,v in resource.openstack_compute_keypair_v2.demo-keys: v.name if strcontains(v.name,"primary") ][0]
  security_groups = ["default",resource.openstack_networking_secgroup_v2.sshaccess.name]

  network {
    port   = resource.openstack_networking_port_v2.pgport[each.value].id
  }

  block_device {
  uuid             = resource.openstack_blockstorage_volume_v3.pgvolume[index(var.pgnodes,each.value)].id
  source_type      = "volume"
  destination_type = "volume"
  }
  user_data       = templatefile("pgnodes-user.tpl",{keys = values(resource.openstack_compute_keypair_v2.demo-keys), cosmosdeb = data.local_file.cosmos.content_base64})
}

# data "openstack_networking_network_v2" "private" {
#   name = var.network
# }

resource "openstack_networking_port_v2" "pgport" {
  name                = "${each.value}_port"
  # We create as many ports as there is servers in the list
  for_each            = toset(var.pgnodes)
  # We use the isolated network for pgnodes
  network_id          = resource.openstack_networking_network_v2.privnet[4].id
  # A list of security group ID
  security_group_ids  = [resource.openstack_networking_secgroup_v2.sshaccess.id, data.openstack_networking_secgroup_v2.defaultsg.id, resource.openstack_networking_secgroup_v2.pgserver.id ]
  admin_state_up      = "true"
  fixed_ip  {
  subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[4].id
  ip_address          = "10.0.5.${index(var.pgnodes, each.value)+10}"
  }
}

resource "openstack_blockstorage_volume_v3" "pgvolume" {
  count       = length(var.pgnodes)
  region      = "RegionOne"
  name        = "pgvolume${count.index}"
  description = "pgvolume"
  size        = 10
  image_id    = resource.openstack_images_image_v2.debian12.id
}
