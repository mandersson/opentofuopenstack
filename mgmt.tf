resource "openstack_compute_instance_v2" "mgmtsrv" {
  name            = "mgmt"  # Instance name of management server

  # We use our uploaded image resource
  image_id        = resource.openstack_images_image_v2.debian12.id

  # And we have datasource of the flavor we want.
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id

  # We use the first uploaded key. Danger server will be recreated if an other key becommes the first.
  key_pair        =  [ for k,v in resource.openstack_compute_keypair_v2.demo-keys: v.name if strcontains(v.name,"primary") ][0]

  # A list of security groups (name or id)
  security_groups = ["default",resource.openstack_networking_secgroup_v2.sshaccess.name]

  network {
    name = var.network # We keep the management server on the default private network
  }
  dynamic "network" {
    for_each = resource.openstack_networking_port_v2.mgmtprivnetport
    content {
      port   = resource.openstack_networking_port_v2.mgmtprivnetport[index(resource.openstack_networking_port_v2.mgmtprivnetport,network.value)].id
    }
  }
  user_data       = templatefile("mgmt-user.tpl",{keys = values(resource.openstack_compute_keypair_v2.demo-keys), cosmosdeb = data.local_file.cosmos.content_base64})
}

# Create a management port on every private network.
resource "openstack_networking_port_v2" "mgmtprivnetport" {
  name                = "mgmtprivnet${count.index}_port"
  # We create as many ports as there is priavte networks.
  count               = 5
  # We loop throug every isolated private network.
  network_id          = resource.openstack_networking_network_v2.privnet[count.index].id
  # We allow egress trafic with the default security group
  security_group_ids  = [ data.openstack_networking_secgroup_v2.defaultsg.id ]
  admin_state_up      = "true"
  fixed_ip  {
    subnet_id           = resource.openstack_networking_subnet_v2.privsubnet[count.index].id
    ip_address          = "10.0.${count.index+1}.8"
  }
}



# Create a floating IP.
resource "openstack_networking_floatingip_v2" "fip1" {
  pool = "public"
}

# Associate the floating IP with the management server
resource "openstack_compute_floatingip_associate_v2" "fip1" {
  floating_ip = openstack_networking_floatingip_v2.fip1.address
  instance_id = openstack_compute_instance_v2.mgmtsrv.id
}

# Output private IP Address of management server
output "serverip" {
 value = openstack_compute_instance_v2.mgmtsrv.access_ip_v4
}

# And the public floating ip address of the management server
output "server_floating_ip" {
 value = openstack_networking_floatingip_v2.fip1.address
}
