#!/bin/bash
if ! pvs | grep '/dev/vdb' > /dev/null 
then 
  vgcreate backupvg /dev/vdb
  lvcreate -n lvol_backup -l 100%FREE backupvg
  mkfs -t xfs -n ftype=1 /dev/backupvg/lvol_backup 
fi
VOLID="$(blkid | awk '{ if ( $1 == "/dev/mapper/backupvg-lvol_backup:") print $2 }' | tr -d \")"
if [[ -n "${VOLID}" && -z "$(grep ${VOLID} /etc/fstab)" ]]
then
  mkdir /backup
  cp /etc/fstab /root/fstab.bak
  echo "${VOLID}   /backup  xfs   defaults     0 0" >> /etc/fstab
fi
systemctl daemon-reload
if ! findmnt --verify
then
  cp /root/fstab.bak /etc/fstab
fi
mount | grep '\s/backup\s' >/dev/null || mount /backup
