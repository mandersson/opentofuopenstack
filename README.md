## Opentofu Openstack example
This project contains a demo openstack environment that can be deployed by opentofu (fork of terraform).
It is tested in a standard devstack deployment
You can source application credentials for the desired target project to deploy in.
Environment variables will be picked up by the openstack provider.
