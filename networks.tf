resource "openstack_networking_network_v2" "privnet" {
  name           = "privnet${count.index}"
  count          = 5
  admin_state_up = "true"
}

# Create 5 private subnets (privsubnet[0]-privsubnet[4])
resource "openstack_networking_subnet_v2" "privsubnet" {
  name        = "privsubnet"
  count       =  5
  network_id  = openstack_networking_network_v2.privnet[count.index].id
  cidr        = "10.0.${count.index+1}.0/24"
  enable_dhcp = "true"
  ip_version  = 4
}

# Connect 3 of our new private subnets to router1 (connected to network public)
# To be able to access floating ip:s
# We keep two networks isolated

resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = data.openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.privsubnet[count.index].id
  count = 5
}


#resource "openstack_networking_router_v2" "routerprivnet3" {
#  name                = "routerprivnet3"
#  admin_state_up      = true
#}

#resource "openstack_networking_router_interface_v2" "routerprivnet3_interface" {
#  router_id = resource.openstack_networking_router_v2.routerprivnet3.id
#  subnet_id = openstack_networking_subnet_v2.privsubnet[3].id
#}

#resource "openstack_networking_router_v2" "routerprivnet4" {
#  name                = "routerprivnet4"
#  admin_state_up      = true
#}

#resource "openstack_networking_router_interface_v2" "routerprivnet4_interface" {
#  router_id = resource.openstack_networking_router_v2.routerprivnet4.id
#  subnet_id = openstack_networking_subnet_v2.privsubnet[4].id
#}

# Datasource for router to connect privete to public networks.
data "openstack_networking_router_v2" "router" {
  name = "router1"
}
